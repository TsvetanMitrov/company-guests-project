import React from 'react';
import { getDistanceFromLatLonInKm } from '../../helpers/calculateDistance';
import './CompanyGuests.css';

const CompanyGuests = ({ officeLat, officeLong, partnersData }) => {

    const partnersAllData = partnersData.map(partner => {
        partner["kilometers"] =
        getDistanceFromLatLonInKm(
            officeLat,
            officeLong,
            partner.latitude,
            partner.longitude
        );
        return partner;
    });

    const partnersKilometersFilter = partnersAllData.filter(partner => partner.kilometers <= 100);
    const partnersIdSort = partnersKilometersFilter.sort((a, b) => a.partner_id - b.partner_id);

    return (
        <div className="partners-container">
            <h1>Partners within 100 km</h1>
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {partnersIdSort.map(partner =>
                        <tr key={partner.partner_id}>
                            <td>{partner.partner_id}</td>
                            <td>{partner.name}</td>
                        </tr>
                    )}
                </tbody>
           </table>
        </div>
    );
}

export default CompanyGuests;