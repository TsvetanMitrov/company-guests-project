export const getDistanceFromLatLonInKm = (officeLat,officeLong,partnerLat,partnerLong) => {
    var earthRadius = 6371;
    var dLat = degreseToRadians(partnerLat-officeLat);
    var dLong = degreseToRadians(partnerLong-officeLong);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(degreseToRadians(officeLat)) * Math.cos(degreseToRadians(partnerLat)) *
        Math.sin(dLong/2) * Math.sin(dLong/2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var kilometers = earthRadius * c;

    return Number(kilometers.toFixed(2));
}

const degreseToRadians = (deg) => {
    return deg * (Math.PI/180);
}