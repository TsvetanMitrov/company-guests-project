import React from 'react';
import CompanyGuests from './components/CompanyGuests/CompanyGuests';
import partnersData from './mocks/partners-data.json';
import './App.css';

const App = () => {
  return (
    <div className="app-container">
        <CompanyGuests officeLat={42.6665921} officeLong={23.351723} partnersData={partnersData}/>;
    </div>
  );
}

export default App;
