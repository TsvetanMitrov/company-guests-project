import partnersData from '../mocks/partners-data.json';

test('match snapshot partners data', () => {
   expect(partnersData).toMatchSnapshot();
});

test('partners data length', () => {
   expect(partnersData).toHaveLength(32);
});