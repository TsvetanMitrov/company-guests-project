import { getDistanceFromLatLonInKm } from '../helpers/calculateDistance';

test('distance to be number bigger than zero', () => {
    expect(getDistanceFromLatLonInKm(42.6665921, 23.351723, 42.7034111, 23.4862259)).toBeGreaterThan(0);
});