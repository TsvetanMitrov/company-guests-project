import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CompanyGuests from '../components/CompanyGuests/CompanyGuests';
import partnersData from '../mocks/partners-data.json';
import renderer  from 'react-test-renderer';

configure({adapter: new Adapter()});
describe('company guests', () => {
    const wrapper = mount(<CompanyGuests officeLat={42.6665921} officeLong={23.351723} partnersData={partnersData} />);
    const table = wrapper.find('table');
    const row = table.find('tr');

    test("match snapshot company guests ", () => {
        const data = renderer.create(<CompanyGuests officeLat={42.6665921} officeLong={23.351723} partnersData={partnersData} />).toJSON();
        expect(data).toMatchSnapshot();
    });

    test('table', () => {
        expect(table).toHaveLength(1);
        expect(row).toHaveLength(24);
    });
});